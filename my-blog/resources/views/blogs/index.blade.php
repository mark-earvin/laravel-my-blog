@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3>Hello There!!</h3>
            <ul>
                @foreach ($blogs as $blog)
                    <li><a href="/blogs/{{ $blog->id }}">{{ $blog->title }}</a></li>
                @endforeach
            </ul>
            <a href="/blogs/create"><span class="btn btn-primary pull-left">New Blog</span></a>
        </div>
    </div>
</div>
@endsection
