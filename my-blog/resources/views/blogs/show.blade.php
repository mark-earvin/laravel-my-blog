@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <h4>{{ $blog->title }} - {{ $blog->user->email }}</h4>
            </div>
            <div class="row container">
                <div class="col-md-8">
                    {{ $blog->body }}
                </div>
            </div>
            <hr>
            <div class="row container">
                <h4>Comments</h4>
                @foreach ($blog->comments as $comment)
                    <div class="row container">
                        <label style="font-style: italic; color: gray;">{{ $comment->user->email }}</label>
                        <br/>
                        <label>{{ $comment->body }}</label>
                    </div>
                @endforeach
                <div class="col-md-6">
                    <hr>
                    <form class="form-group" role="form" method="POST" action="/comments/store">

                        <div class="form-group">
                            <textarea name="body" class="form-control">{{ old('body') }}</textarea>
                        </div>

                        <div class="form-group" hidden>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>

                        <div class="form-group" hidden>
                            <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Comment</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
