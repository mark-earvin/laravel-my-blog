@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3>
            Add your blog here:
        </h3>
        <div class="col-md-8 col-md-offset-2">
            <form class="form-group" role="form" method="POST" action="/blogs/store">

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control">{{ old('title') }}</input>
                </div>

                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" class="form-control">{{ old('body') }}</textarea>
                </div>

                <div class="form-group" hidden>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add Blog</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
