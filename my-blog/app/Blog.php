<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function by(User $user){
        $this->user_id = $user->id;
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function addComment(Comment $comment){
        return $this->comments()->save($comment);
    }
}
