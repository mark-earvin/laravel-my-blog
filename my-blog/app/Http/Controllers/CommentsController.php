<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Blog;
use App\Comment;

class CommentsController extends Controller
{
    //
    public function store(Request $request){
        // return view('blogs.create');

        $this->validate($request, [
            "body" => "required|min:5",
        ]);

        $user = Auth::user();
        $blog = Blog::find($request->blog_id);
        $comment = new Comment($request->all());
        $comment->by($user);
        $comment->blog_id = $blog->id;
        $blog->addComment($comment);

        return back();
    }
}
