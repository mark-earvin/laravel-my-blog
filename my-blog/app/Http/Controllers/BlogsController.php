<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Blog;

class BlogsController extends Controller
{
    //
    public function index(){
        $blogs = Blog::all();
        return view('blogs.index', compact('blogs'));
    }

    public function create(){
        return view('blogs.create');
        // return Auth::user();
    }

    public function store(Request $request){
        // return view('blogs.create');

        $this->validate($request, [
            "title" => "required|min:3",
            "body" => "required|min:3",
        ]);

        $user = Auth::user();
        $blog = new Blog($request->all());
        $blog->by($user);
        $user->addBlog($blog);

        // return back();
        return redirect()->action('BlogsController@index');
    }

    public function show(Blog $blog){

        $blog->load('comments.user');

        return view('blogs.show', compact('blog'));
    }
}
